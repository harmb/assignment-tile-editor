module Tile exposing (view)

{-| A module to render a simple tile.

Elm documentation: <https://elm-lang.org/docs>

Documentation of imported modules:

  - <https://package.elm-lang.org/packages/elm/core/latest/>
  - <https://package.elm-lang.org/packages/elm/html/latest/Html>
  - <https://package.elm-lang.org/packages/elm/html/latest/Html-Attributes>

-}

import Html exposing (Html)
import Html.Attributes


{-| A simple view to render a tile.
-}
view :
    { url : String
    , name : String
    }
    -> Html msg
view config =
    Html.a
        [ Html.Attributes.href config.url
        , Html.Attributes.target "_blank"

        -- Note classes are defined in `public/styles.css`
        -- Styles can also be defined with `Html.Attributes.style`
        , Html.Attributes.class "tile"
        ]
        [ Html.text config.name
        ]
