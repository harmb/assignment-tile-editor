module Main exposing (main)

{-| The main module for the tile editor application.

Elm documentation: <https://elm-lang.org/docs>

Documentation of imported modules:
- <https://package.elm-lang.org/packages/elm/core/latest/>
- <https://package.elm-lang.org/packages/elm/browser/latest/Browser>
- <https://package.elm-lang.org/packages/elm/html/latest/Html>
- <https://package.elm-lang.org/packages/elm/html/latest/Html-Attributes>
- <https://package.elm-lang.org/packages/elm/html/latest/Html-Events>

-}

import Browser
import Html
import Html.Attributes
import Html.Events
import Tile


{-| This is the entry point for the Elm compiler and sets up our tile editor application.
See the individual functions below for more info.
-}
main : Program Flags Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


{-| Flags are values that you can pass in from JavaScript when initializing the application.
The can ignore these for now.
-}
type alias Flags =
    ()


{-| The `Model` defines the state of the application.
-}
type alias Model =
    { url : String
    , name : String
    }


{-| Initialize the application state and any side-effects (currently none, you can probably ignore the side-effects).
This is called when the application (browser page) is loaded.
-}
init : Flags -> ( Model, Cmd Msg )
init _ =
    ( { url = "https://www.symbaloo.com"
      , name = "Symbaloo"
      }
    , Cmd.none
    )


{-| Render the application in the browser.
This function is called whenever the `Model` changes (after `init` or `update`).
-}
view : Model -> Browser.Document Msg
view model =
    { title = "Symbaloo Tile Editor"
    , body =
        [ -- render an input field for the url of the tile
          Html.div
            []
            [ Html.input
                [ Html.Attributes.type_ "url"
                , Html.Attributes.placeholder "url"
                , Html.Attributes.value model.url
                , Html.Events.onInput UserChangedUrl
                ]
                []
            ]
        , -- render an input field for the name of the tile
          Html.div
            []
            [ Html.input
                [ Html.Attributes.type_ "text"
                , Html.Attributes.placeholder "name"
                , Html.Attributes.value model.name
                , Html.Events.onInput UserChangedName
                ]
                []
            ]
        , -- render a preview of the tile
          Html.div []
            [ Tile.view
                { name = model.name
                , url = model.url
                }
            ]
        ]
    }


{-| Our messages for the application. These are basically callback functions.
For instance in the `view` you can set up an event when a user changes an input field by (see also above):

    Html.Events.onInput UserChangedName

-}
type Msg
    = UserChangedUrl String
    | UserChangedName String


{-| Whenever a message is produced by the user (click, input, etc.) or the browser (resize, timeout, http response, etc.)
the `update` function gets called.
Here you get the message and the current state of the application (model),
and you return the new state and any side-effects (currently none, you can probably ignore the side-effects).
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UserChangedUrl newUrl ->
            ( { model | url = newUrl }, Cmd.none )

        UserChangedName newName ->
            ( { model | name = newName }, Cmd.none )


{-| Subscriptions are listeners that produce messages.
You can probably ignore them for now.

You can use subscriptions for instance for intervals, browser resizes, etc.
They are declarative and based on the current state of the application.
So this function is called whenever the `Model` changes (after `init` or `update`).

-}
subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
