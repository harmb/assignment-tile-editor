# Symbaloo Tile Editor

At Symbaloo everything evolves around tiles. This is a little assignment to extend/improve a very basic tile editor.

## Goal

The main goal is that you create a bit of code so we can talk about it. It doesn't have to be perfect,
and you don't have to spend too much time on it.

See if you can improve and extend the provided tile editor a bit.
You can have a look at our existing tile editor, or go in your own direction. Whatever you like.

## Setup

This assignment is in Elm. The Elm compiler will compile the Elm code to JavaScript, which we can run in the browser.
Elm code is quite a bit different from JavaScript code. So it might take a bit of effort to get into it.
You can have a look at the code straight away (it includes comments) or have a look at the resources below.

You will need to install the Elm compiler:

- https://elm-lang.org/
- you can also install Elm through npm: `npm install -g elm`

We recommend using vscode with the Elm tooling plugin and elm-format, but you can also use any editor you prefer.

- vscode: https://code.visualstudio.com/
- elm tooling plugin: https://marketplace.visualstudio.com/items?itemName=Elmtooling.elm-ls-vscode
- elm-format: https://github.com/avh4/elm-format

Files:

- `elm.json` - the Elm project configuration, you probably don't need to change this.
- `build.bat` - will build the Elm application on Windows. If you're not on Windows you can use:
  ```bash
  elm make src/Main.elm --output=public/main.js --debug
  ```
  This wil generate the `public/main.js` file with all the JavaScript code.
- `src/Main.elm` - the entry point of our Elm tile editor. You can write most of the code here.
- `src/Tile.elm` - a module for rendering a tile, which you can also edit.
- `public/styles.css` - the CSS for the page. Feel free to edit.
- `public/index.html` - you can open this file in your browser to view the tile editor.
  First you will have to build the Elm application for it to work.
  Note there is a blue rectangle with the Elm logo on the bottom right of the page.
  This is the Elm debugger. If you click it you can view the state of the application and also rewind it.
  If you don't want it just remove the `--debug` flag from the `elm make` command.

## Resources

- Elm: https://elm-lang.org/
- Elm docs: https://elm-lang.org/docs
- Elm guide: https://guide.elm-lang.org/
- Elm syntax: https://elm-lang.org/docs/syntax
- Elm compared to JavaScript: https://elm-lang.org/docs/from-javascript
- Elm examples: https://elm-lang.org/examples
- Elm packages: https://package.elm-lang.org/ (which also include the core packages and documentation)
- Elm Slack: https://elmlang.herokuapp.com/ (a friendly Slack where you can ask any questions;
  I'm also `harmb` on there, feel free to send me a message)
